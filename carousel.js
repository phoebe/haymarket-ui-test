$(document).ready(function() {   
        $('.carousel-ul li:first').before($('.carousel-ul li:last'));  
  
        $('#right-scroll .arrow-right').click(function(){  
    
            var item_width = $('.carousel-ul li').outerWidth() + 10;  
            var left_indent = parseInt($('.carousel-ul').css('left')) - item_width;  
    
            $('.carousel-ul').animate({'left' : left_indent},{queue:false, duration:500},function(){  
   
                $('.carousel-ul li:last').after($('.carousel-ul li:first'));  
   
                $('.carousel-ul').css({'left' : '-210px'});  
            });  
        });  
  
        $('#left-scroll .arrow-left').click(function(){  
  
            var item_width = $('.carousel-ul li').outerWidth() + 10;   
            var left_indent = parseInt($('.carousel-ul').css('left')) + item_width;  
  
            $('.carousel-ul').animate({'left' : left_indent},{queue:false, duration:500},function(){  
 
            $('.carousel-ul li:first').before($('.carousel-ul li:last'));  

            $('.carousel-ul').css({'left' : '-210px'});  
            });  
  
        });  
 }); 